import { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';


export default function Logout(){
  
  const { unsetUser, setUser} = useContext(UserContext);


  unsetUser();

  useEffect(() => {
    setUser({id:null})
  }, [])
  
  // localStorage.clear();
  
  return (

    // clear() removes the data stored in the localStorage

   

    // redirect or navigate the user back to the login page
    <Navigate to="/" />
  )
}